const today = new Date()
const day = today.getDate()
const dayOfTheWeek = today.getDay()
const dayList = [
    'Saturday', 
    'Sunday', 
    'Monday', 
    'Tuesday', 
    'Wednesday', 
    'Thursday', 
    'Friday'
]
const dayOfTheMonth = today.getDay()
const month = today.getMonth()
const monthList = [
    'January', 
    'February', 
    'March', 
    'April', 
    'May', 
    'June', 
    'July', 
    'August', 
    'September', 
    'November', 
    'December'
]
const hour = today.getHours()
const minute = today.getMinutes()
const seconds = today.getSeconds()
const year = today.getFullYear()

console.log(`Today is ${dayList[dayOfTheWeek]}`)
console.log(`${monthList[month]} ${day}, ${year}`)
console.log(`Current time is ${hour}:${minute}:${seconds}`)