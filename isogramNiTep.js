function checkIfIsogram(str){

 const antiCaseSensitive = str.toLowerCase();   
 const isogram = antiCaseSensitive.split('').filter((item, pos, arr) => arr.indexOf(item) == pos).length == str.length
 console.log(isogram)

}

checkIfIsogram('Dermatoglyphics')
checkIfIsogram('aba')
checkIfIsogram('moOse')