function isIsogram(str) {
    for (i = 0; i < str.length; i++) {
        for (j = 0; j < str.length; j++) {
            if (j != i) {
                if (str[j].toLowerCase() == str[i].toLowerCase()) {
                    return false;
                }
            }
        }
    }
    return true;
}

console.log(isIsogram("Dermatoglyphics"));
console.log(isIsogram("aba"));
console.log(isIsogram("moOse"));